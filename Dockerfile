## initial stage
FROM node:16-alpine3.13 AS initial-stage

RUN apk --no-cache add  \
      autoconf \
      automake \
      bash \
      build-base \
      ca-certificates \
      curl \
      file \
      g++ \
      gcc \
      git \
      lcms2-dev \
      libjpeg-turbo-dev \
      libpng-dev \
      make \
      nasm \
      wget \
      zlib-dev

# WORKDIR
WORKDIR /usr/src/nuxt-app

COPY package*.json ./

RUN npm cache clean --force
RUN npm install
# RUN npm audit fix

COPY . /usr/src/nuxt-app/

RUN npm run build

# expose 3000 on container
EXPOSE 3000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=3000

CMD ["npm", "run", "prod"]
