// import moment from 'moment'
import userService from '@/services/userService'

export const state = () => ({
  user: null,
  loading: false
})

export const mutations = {
  startRequest(state) {
    state.loading = true
  },
  requestFailed(state) {
    state.loading = false
  },
  setUser(state, { user }) {
    state.loading = false
    state.user = user

    /*
    if (moment(state.user.last_login_at).isValid()) {
      state.user.last_login_at_formatted = moment(
        state.user.last_login_at
      ).format(process.env.FORMAT_DATETIME)
    } else {
      state.user.last_login_at_formatted = 'Never logged in'
    }
    */

    if (state.user.firstName && state.user.lastName) {
      state.user.fullName = `${state.user.firstName}, ${state.user.lastName}`
    }
  }
}

export const actions = {
  me({ dispatch, commit }, { router }) {
    dispatch('alert/clear', {}, { root: true })
    commit('startRequest')

    userService
      .me()
      .then((response) => {
        commit('setUser', { user: response })
      })
      .catch((e) => {
        commit('requestFailed')
        dispatch('common/handleServiceException', { e, router }, { root: true })
      })
  },
  updateMe({ dispatch, commit }, { user, router }) {
    dispatch('alert/clear', {}, { root: true })
    commit('startRequest')

    userService
      .updateMe({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        phone: user.phone,
        password: user.password
      })
      .then((response) => {
        commit('setUser', { user: response })
        commit(
          'alert/setMessage',
          {
            type: 'success',
            message: 'Your information has been successfully updated.'
          },
          { root: true }
        )
      })
      .catch((e) => {
        commit('requestFailed')
        dispatch('common/handleServiceException', { e, router }, { root: true })
      })
  }
}

export const getters = {}
