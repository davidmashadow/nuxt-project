import axios from 'axios'

export default {
  me() {
    return axios
      .get(`${process.env.API_URL}/api/account/user`, {})
      .then((response) => {
        return response.data
      })
  },
  updateMe(me) {
    return axios
      .put(`${process.env.API_URL}/api/account/user`, me)
      .then((response) => {
        return response.data
      })
  }
}
