import axios from 'axios'

export default {
  passwordReset({ email, key, password }) {
    return axios
      .post(`${process.env.API_URL}/api/public/complete-password-reset`, {
        email,
        key,
        password
      })
      .then((response) => {
        return response.data
      })
  },

  passwordResetRequest({ email }) {
    return axios
      .post(`${process.env.API_URL}/api/public/password-reset`, {
        email
      })
      .then((response) => {
        return response.data
      })
  },

  register({ firstName, lastName, email, phone, password }) {
    return axios
      .post(`${process.env.API_URL}/api/public/register`, {
        firstName,
        lastName,
        email,
        phone,
        password
      })
      .then((response) => {
        return response.data
      })
  },

  checkActivation(email) {
    console.log(`email => ${email}`)
    return axios
      .get(`${process.env.API_URL}/api/public/check-activation?email=${email}`)
      .then((response) => {
        return response
      })
      .catch((e) => {
        throw e
      })
  },

  login(email, password) {
    return axios
      .post(`${process.env.API_URL}/api/authenticate`, {
        email,
        password
      })
      .then((response) => {
        return response.data
      })
      .catch((e) => {
        throw e
      })
  }
}
