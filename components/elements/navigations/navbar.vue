<template>
  <!-- BEGIN: Header-->
  <nav
    class="
      header-navbar
      navbar-expand-lg
      navbar
      navbar-with-menu
      navbar-fixed
      navbar-shadow
      navbar-brand-center
    "
  >
    <div class="navbar-header d-xl-block d-none">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item">
          <nuxt-link :to="{ path: '/' }" tag="a" class="navbar-brand">
            <div class="brand-logo" />
          </nuxt-link>
        </li>
      </ul>
    </div>
    <div class="navbar-wrapper">
      <div class="navbar-container content">
        <div id="navbar-mobile" class="navbar-collapse">
          <div
            class="
              mr-auto
              float-left
              bookmark-wrapper
              d-flex
              align-items-center
            "
          >
            <!--
            <ul class="nav navbar-nav">
              <li class="nav-item mobile-menu d-xl-none mr-auto">
                <a
                  class="nav-link nav-menu-main menu-toggle hidden-xs"
                  href="#"
                >
                  <i class="ficon feather icon-menu" />
                </a>
              </li>
            </ul>
            -->
            <ul class="nav navbar-nav bookmark-icons">
              <li class="nav-item d-none d-lg-block">
                <nuxt-link
                  :to="{ path: `${localePath('/auth/login')}` }"
                  class="nav-link"
                  data-toggle="tooltip"
                  data-placement="top"
                  :title="$t('topMenu.login')"
                >
                  <i class="ficon feather icon-user" />
                </nuxt-link>
              </li>
              <li class="nav-item d-none d-lg-block">
                <nuxt-link
                  :to="{ path: `${localePath('/auth/register')}` }"
                  class="nav-link"
                  data-toggle="tooltip"
                  data-placement="top"
                  :title="$t('topMenu.register')"
                >
                  <i class="ficon feather icon-user-plus" />
                </nuxt-link>
              </li>
              <li class="nav-item d-none d-lg-block">
                <nuxt-link
                  :to="{ path: `${localePath('/auth/forgotPassword')}` }"
                  class="nav-link"
                  data-toggle="tooltip"
                  data-placement="top"
                  :title="$t('topMenu.resetPassword')"
                >
                  <i class="ficon feather icon-user-x" />
                </nuxt-link>
              </li>
              <!--
              <li class="nav-item d-none d-lg-block">
                <a
                  class="nav-link"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Calendar"
                >
                  <i class="ficon feather icon-log-out" />
                </a>
              </li>
              -->
            </ul>
            <!--
            <ul class="nav navbar-nav">
              <li class="nav-item d-none d-lg-block">
                <a class="nav-link bookmark-star">
                  <i class="ficon feather icon-star warning" />
                </a>
                <div class="bookmark-input search-input">
                  <div class="bookmark-input-icon">
                    <i class="feather icon-search primary" />
                  </div>
                  <input
                    class="form-control input"
                    type="text"
                    placeholder="Explore Vuexy..."
                    tabindex="0"
                    data-search="template-list"
                  />
                  <ul class="search-list search-list-bookmark" />
                </div>
              </li>
            </ul>
            -->
          </div>
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-language nav-item">
              <a
                id="dropdown-flag"
                class="dropdown-toggle nav-link"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <i :class="`flag-icon flag-icon-${this.$i18n.locale}`" />
                <span class="selected-language">{{ $t('language') }}</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                <nuxt-link
                  v-for="(locale, index) in showLocales"
                  :key="index"
                  :to="switchLocalePath(locale.code)"
                  tag="a"
                  class="dropdown-item"
                >
                  <i :class="`flag-icon flag-icon-${locale.code}`" />
                  {{ locale.name }}
                </nuxt-link>
              </div>
            </li>
            <!--
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link nav-link-expand">
                <i class="ficon feather icon-maximize" />
              </a>
            </li>
            -->
            <!--
            <li class="nav-item nav-search">
              <a class="nav-link nav-link-search">
                <i class="ficon feather icon-search" />
              </a>
              <div class="search-input">
                <div class="search-input-icon">
                  <i class="feather icon-search primary" />
                </div>
                <input
                  class="input"
                  type="text"
                  placeholder="Explore Vuexy..."
                  tabindex="-1"
                  data-search="template-list"
                />
                <div class="search-input-close">
                  <i class="feather icon-x" />
                </div>
                <ul class="search-list search-list-main" />
              </div>
            </li>
            -->
            <li
              v-if="isLoggedIn() === true"
              class="dropdown dropdown-notification nav-item"
            >
              <a
                class="nav-link nav-link-label"
                href="#"
                data-toggle="dropdown"
              >
                <i class="ficon feather icon-bell" />
                <span class="badge badge-pill badge-primary badge-up">1</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <div class="dropdown-header m-0 p-2">
                    <h3 class="white">1 New</h3>
                    <span class="notification-title">App Notifications</span>
                  </div>
                </li>
                <li class="scrollable-container media-list">
                  <a
                    class="d-flex justify-content-between"
                    href="javascript:void(0)"
                  >
                    <div class="media d-flex align-items-start">
                      <div class="media-left">
                        <i
                          class="feather icon-plus-square font-medium-5 primary"
                        />
                      </div>
                      <div class="media-body">
                        <h6 class="primary media-heading">
                          You have new order!
                        </h6>
                        <small class="notification-text">
                          Are your going to meet me tonight?
                        </small>
                      </div>
                      <small>
                        <time
                          class="media-meta"
                          datetime="2015-06-11T18:29:20+08:00"
                        >
                          9 hours ago
                        </time>
                      </small>
                    </div>
                  </a>
                </li>
                <li class="dropdown-menu-footer">
                  <a
                    class="dropdown-item p-1 text-center"
                    href="javascript:void(0)"
                  >
                    Read all notifications
                  </a>
                </li>
              </ul>
            </li>
            <li
              v-if="isLoggedIn() === true"
              class="dropdown dropdown-user nav-item"
            >
              <a
                class="dropdown-toggle nav-link dropdown-user-link"
                href="#"
                data-toggle="dropdown"
              >
                <div class="user-nav d-sm-flex d-none">
                  <span class="user-name text-bold-600">
                    {{ user.sub }}
                  </span>
                  <span class="user-status">{{ user.auth }}</span>
                </div>
                <span>
                  <img
                    class="round"
                    src="@/static/images/portrait/small/avatar-s-11.jpg"
                    alt="avatar"
                    height="40"
                    width="40"
                  />
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <nuxt-link
                  :to="{ path: `${localePath('/accountsettings')}` }"
                  tag="a"
                  class="dropdown-item"
                >
                  <i class="feather icon-user" />
                  Edit Profile
                </nuxt-link>
                <a class="dropdown-item">
                  <i class="feather icon-mail" />
                  My Inbox
                </a>
                <a class="dropdown-item">
                  <i class="feather icon-check-square" />
                  Task
                </a>
                <a class="dropdown-item">
                  <i class="feather icon-message-square" />
                  Chats
                </a>
                <div class="dropdown-divider" />
                <nuxt-link
                  :to="{ path: `${localePath('/logout')}` }"
                  class="dropdown-item"
                  data-cy="nav-bar-logout"
                  link-classes="btn btn-grey  mx-1 text-dark"
                >
                  <i class="feather icon-power" />
                  Logout
                </nuxt-link>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!--
    <ul class="main-search-list-defaultlist d-none">
      <li class="d-flex align-items-center">
        <a class="pb-25">
          <h6 class="text-primary mb-0">Files</h6>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="mr-50">
              <img src="@/static/images/icons/xls.png" alt="png" height="32" />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Two new item submitted</p>
              <small class="text-muted">Marketing Manager</small>
            </div>
          </div>
          <small class="search-data-size mr-50 text-muted">&apos;17kb</small>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="mr-50">
              <img src="@/static/images/icons/jpg.png" alt="png" height="32" />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">52 JPG file Generated</p>
              <small class="text-muted">FontEnd Developer</small>
            </div>
          </div>
          <small class="search-data-size mr-50 text-muted">&apos;11kb</small>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="mr-50">
              <img src="@/static/images/icons/pdf.png" alt="png" height="32" />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">25 PDF File Uploaded</p>
              <small class="text-muted">Digital Marketing Manager</small>
            </div>
          </div>
          <small class="search-data-size mr-50 text-muted">&apos;150kb</small>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="mr-50">
              <img src="@/static/images/icons/doc.png" alt="png" height="32" />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Anna_Strong</p>
              <small class="text-muted">Web Designer</small>
            </div>
          </div>
          <small class="search-data-size mr-50 text-muted">&apos;256kb</small>
        </a>
      </li>
      <li class="d-flex align-items-center">
        <a class="pb-25" href="#">
          <h6 class="text-primary mb-0">Members</h6>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar mr-50">
              <img
                src="@/static/images/portrait/small/avatar-s-8.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">John Doe</p>
              <small class="text-muted">UI designer</small>
            </div>
          </div>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar mr-50">
              <img
                src="@/static/images/portrait/small/avatar-s-1.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Michal Clark</p>
              <small class="text-muted">FontEnd Developer</small>
            </div>
          </div>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar mr-50">
              <img
                src="@/static/images/portrait/small/avatar-s-14.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Milena Gibson</p>
              <small class="text-muted">Digital Marketing Manager</small>
            </div>
          </div>
        </a>
      </li>
      <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar mr-50">
              <img
                src="@/static/images/portrait/small/avatar-s-6.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Anna Strong</p>
              <small class="text-muted">Web Designer</small>
            </div>
          </div>
        </a>
      </li>
    </ul>
    <ul class="main-search-list-defaultlist-other-list d-none">
      <li
        class="
          auto-suggestion
          d-flex
          align-items-center
          justify-content-between
          cursor-pointer
        "
      >
        <a
          class="d-flex align-items-center justify-content-between w-100 py-50"
        >
          <div class="d-flex justify-content-start">
            <span class="mr-75 feather icon-alert-circle" /><span
              >No results found.</span
            >
          </div>
        </a>
      </li>
    </ul>
  -->
</template>

<script>
import { mapGetters, mapState } from 'vuex'

export default {
  name: 'NavBar',
  computed: {
    ...mapState('auth', ['user']),
    ...mapGetters('auth', ['isLoggedIn']),

    showLocales() {
      return this.$i18n.locales.filter(
        (locale) => locale.code !== this.$i18n.locale
      )
    }
  },
  mounted() {
    console.log(this.$i18n)
  }
}
</script>
