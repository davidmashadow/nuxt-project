import packageJson from '../package.json'

export default (_, inject) => {
  inject('nuxtI18nVersion', packageJson.dependencies['@nuxtjs/i18n'])
}
