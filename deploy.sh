#!/bin/bash

echo "STEP1 TIME: $(date)"
echo " git pull origin develop "
git pull origin develop

echo "STEP2 TIME: $(date)"
echo " docker-compose down "
docker-compose down

echo "STEP3 TIME: $(date)"
echo " docker rmi asinpan-web-test-image:latest "
docker rmi asinpan-web-test-image:latest

echo "STEP4 TIME: $(date)"
echo " docker-compose up -d "
docker-compose up -d
