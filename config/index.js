import us from '../lang/en-US.js'
import tr from '../lang/tr-TR.js'

export const API_ROOT = 'https://jsonplaceholder.typicode.com/'

export const I18N = {
  locales: [
    {
      code: 'us',
      iso: 'en-US',
      name: 'English'
    },
    {
      code: 'tr',
      iso: 'tr-TR',
      name: 'Turkish'
    }
  ],
  defaultLocale: 'us',
  /*
  routes: {
    about: {
      us: '/about-us',
      tr: '/hakkimizda'
    },
    posts: {
      us: '/posts',
      fr: '/makaleler'
    },
    'post/_id': {
      us: '/article/:id?',
      tr: '/gonderi/:id?'
    },
    'dynamicNested/_category': {
      tr: 'imbrication-dynamique/:category'
    }
  },
  */
  vueI18n: {
    fallbackLocale: 'us',
    messages: { us, tr }
  }
}
