import { API_ROOT, I18N } from './config'

module.exports = {
  telemetry: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s | Vue.js (Frontend)',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          process.env.npm_package_description || 'Frontend made by Nuxt.js'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600'
      }
    ],
    script: [{ src: '/vendors/js/vendors.min.js', body: true }]
  },
  // Customize the progress-bar color
  loading: { color: '#fff' },
  // Global CSS
  css: [
    '@/assets/css/custom.scss',
    '@/static/vendors/css/vendors.min.css',
    // Theme CSS
    '@/static/css/bootstrap.css',
    '@/static/css/bootstrap-extended.css',
    '@/static/css/colors.css',
    '@/static/css/components.css',
    '@/static/css/themes/dark-layout.css',
    '@/static/css/themes/semi-dark-layout.css',
    // General Page CSS
    '@/static/css/core/menu/menu-types/horizontal-menu.css',
    '@/static/css/core/colors/palette-gradient.css'
  ],
  // Plugins to load before mounting the App
  plugins: [
    { src: '@/plugins/vuelidate', ssr: false },
    { src: '@/plugins/version' }
  ],
  // Nuxt.js dev-modules
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module'
  ],
  // Nuxt.js modules
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://nuxtjs.org/docs/configuration-glossary/configuration-modules/#the-modules-property
    '@nuxtjs/pwa',
    // Doc: https://www.npmjs.com/package/@nuxtjs/toast
    '@nuxtjs/toast',
    // Doc: https://google-analytics.nuxtjs.org/
    '@nuxtjs/google-analytics',
    // Doc: https://www.npmjs.com/package/cookie-universal-nuxt
    ['cookie-universal-nuxt', {}],
    // Doc: https://i18n.nuxtjs.org/
    ['@nuxtjs/i18n', I18N],
    // Doc: https://axios.nuxtjs.org/usage
    ['@nuxtjs/axios', { baseURL: API_ROOT }]
  ],
  // Axios module configuration
  // See https://axios.nuxtjs.org/options
  axios: {},
  // Build configuration
  build: {
    // You can extend webpack config here
    extend(_config, _ctx) {}
  },
  env: {
    API_URL: 'https://testapi.asinpan.com',
    BASE_URL: '/',
    FORMAT_DATETIME: 'YYYY-MM-DD HH:MM:SS'
  },
  generate: {
    routes: ['/another-page', '/sample-page/1', '/sample-page/2']
  },
  router: {
    base: process.env.BASE_URL || '/',
    middleware: ['check-auth'],
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'another-page',
        path: '/another-page',
        component: resolve(__dirname, 'pages/page.vue'),
        meta: {
          pageName: 'another page'
        }
      })
      routes.push({
        name: 'sample-page',
        path: '/sample-page/:pageId',
        component: resolve(__dirname, 'pages/page.vue'),
        meta: {
          pageName: 'sample page'
        }
      })
    }
  },
  toast: {
    position: 'top-center',
    register: [
      // Register custom toasts
    ]
  },
  googleAnalytics: {
    id: 'GOOGLE_ANALYTICS_ID'
  },
  server: {
    host: '0.0.0.0',
    port: 3000
  }
}
