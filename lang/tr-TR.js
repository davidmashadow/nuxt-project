export default {
  home: 'Anasayfa',
  language: 'Turkish',
  posts: 'Gönderiler',
  about: 'Hakkımızda',
  homepage: {
    subtitle: 'Su an Türkçe'
  },
  topMenu: {
    searchUrl: 'URL ARAMA',
    paymentUrl: 'ÖDEME SAYFASI',
    home: ' ANASAYFA',
    price: 'ÜCRETLENDİRME',
    contact: 'İLETİŞİM',
    support: 'DESTEK',
    pages: 'KULLANICI İŞLEMLERİ',
    login: 'GİRİŞ',
    register: 'KAYIT OL',
    forgotPassword: 'ŞİFREMİ UNUTTUM',
    resetPassword: 'ŞİFREMİ RESETLE'
  },
  formElements: {
    header: 'Hesap oluştur',
    title: 'Yeni bir hesap oluşturmak için, lütfen aşağıdaki formu doldurunuz.',
    headerLogin: 'Giriş',
    titleLogin: 'Hoşgeldiniz, lütfen E-posta adresiniz ile giriş yapınız.',
    orLogin: 'VEYA',
    headerPasswordReset: 'Şifremi yenile',
    titlePasswordReset: `E-posta adresinizi giriniz. Şifrenizi yenilemeniz için gereken talimatları içeren bir mail göndereceğiz.`,
    headerPassword2: 'Mail gönderildi.',
    titlePassword2: `Şifrenizi yenilemeniz için gereken maili gönderdik. Lütfen E-posta kutunusunu ve spam kutunuzu kontrol ediniz. Mailin gelmesi servis yoğunluğuna bağlı olarak 5 dakikayı bulabilir`,
    titlePassword3: 'Lütfen yeni şifrenizi giriniz.',
    titlePassword4: 'Şifreniz güncellendi.',
    tıtlePassword5: 'Yeni şifreniz ile giriş yapabilirsiniz.',
    buttonPasswordReset: 'Şifremi yenile',
    name: 'Adı',
    surname: 'Soyadı',
    email: 'E-posta adresi',
    phoneNumber: 'Telefon numarası',
    oldPassword: 'Eski şifre',
    password: 'Şifre',
    confirmPassword: 'Şifre tekrarı',
    termsAndConditions: 'Belirtilen şartları ve koşulları kabul ediyorum.',
    loginButton: 'Giriş yap',
    registerButton: 'Kayıt ol',
    forgotPasswordButton: 'Şifremi unuttum',
    rememberMeButton: 'Beni hatırla',
    saveChanges: 'Değişiklikleri kaydet',
    cancel: 'İptal',
    uploadNewPhoto: 'Yeni fotoğraf yükle',
    reset: 'Yenile',
    profilePhotoDefinition:
      'İzin verilen uzantılar JPG, GIF veya PNG. Maks. boyut 800kB.',
    creditCard: 'Kredi Kartı Numarası',
    validityMonth: 'Geçerlilik Tarihi / Ay',
    validityYear: 'Geçerlilik Tarihi / Gün',
    ccv: 'CCV',
    address: 'İkamet Adresiniz',
    city: 'Şehir',
    country: 'Ülke',
    fullName: 'İsim Soyisim',
    payment: 'Ödeme',
    pleaseWait: 'Lütfen bekleyiniz...'
  },
  formTitle: {
    subscriberInfo: 'Abonelik Bilgileri',
    creditCardInfo: 'Kredi Kartı Bilgileri',
    billingInfo: 'Fatura Bilgileri'
  },
  formFeedbacks: {
    name: 'İsim gereklidir.',
    surname: 'Soyisim gereklidir.',
    email: 'Geçersiz E-posta adresi.',
    phone: 'Minimum 10 karakterden oluşan telefon numarası giriniz.',
    password: 'Minimum 6 karakterden oluşan şifre giriniz.',
    rePassword: 'Şifre tekrarınız şifreniz ile eşleşmemektedir.',
    checkbox: 'Belirtilen şart ve koşulları kabul etmelisin.'
  }
}
